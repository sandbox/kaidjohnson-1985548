CONTENTS OF THIS FILE
---------------------

 * Introduction


INTRODUCTION
------------

Current Maintainer: Kai Johnson <kaidjohnson@gmail.com>

Current is a Drupal module that solves the problem of displaying different
types of content with different date-type fields using different sort schemes.
